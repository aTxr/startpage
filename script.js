function clock()
{
    var d = new Date();
    var date = d.getDate();
    var day = d.getDay();
    var month = d.getMonth();
    var hour = d.getHours() +1;
    var min = d.getMinutes();
    var monthArr = ["January", "February","March", "April", "May", "June", "July", "August", "September", "October", "November","December"];
    var dayArr = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturnday"];
    month = monthArr[month];
    day = dayArr[day];
    if (min < 10)
    {
        min = "0"+min;
    }
    document.getElementById("date").innerHTML=day+", "+month+" "+date;
    document.getElementById("hour").innerHTML=hour+":"+min;
}

function display(k)
{
    var div = document.getElementById('link'+k);
    var a = document.getElementsByClassName('link'+k+'_ul');
    div.style.backgroundColor = 'rgb(133, 133, 173, 0.8)';
    div.style.border = 'border: solid 1px black';
    for (var i = 0; i < a.length; i++) {
      a[i].style.color = 'white';
    }
}

function hide(k)
{
    var a = document.getElementsByClassName('link'+k+'_ul');
    var div = document.getElementById('link'+k);
    div.style.backgroundColor = 'transparent';
    div.style.border = 'border: solid 1px transparent';
    for (var i = 0; i < a.length; i++) {
      a[i].style.color = 'transparent';
    }
}
